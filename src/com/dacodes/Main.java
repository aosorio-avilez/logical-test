package com.dacodes;

import java.util.Arrays;
import java.util.stream.IntStream;

public class Main {

    public static void main(String[] args) {
        Console console = new Console();

        try {
            Board[] boards = new Board[console.getNumberOfTestCases()];

            IntStream.range(0, boards.length).forEach(testCase -> {
                int[] dimension = console.getDimensions();
                boards[testCase] = new Board(dimension[0], dimension[1]);
            });

            Arrays.stream(boards).forEach(Board::start);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}
