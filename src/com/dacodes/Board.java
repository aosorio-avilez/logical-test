package com.dacodes;

/**
 * Class representing a board
 */
class Board {

    /**
     * Direction of the movement
     */
    enum Direction {
        RIGHT("R"),
        LEFT("L"),
        DOWN("D"),
        UP("U");

        public String description;

        Direction(String description) {
            this.description = description;
        }
    }

    /**
     * Application board
     */
    private int[][] mBoard;

    /**
     * Number of rows in the board
     */
    private int mRows;

    /**
     * Number of columns in the board
     */
    private int mColumns;

    /**
     * Current row
     */
    private int mRow;

    /**
     * Current column
     */
    private int mColumn;

    /**
     * Current direction
     */
    private Direction mDirection;

    /**
     * Constructor
     *
     * @param columns number of columns in the board
     * @param rows    number of rows in the board
     */
    public Board(int columns, int rows) {
        mColumns = columns;
        mRows = rows;
        setup();
    }

    /**
     * Start the test
     */
    public void start() {
        while (isNotFinished()) moveToNextPoint();
        printDirection();
    }

    /**
     * Initialize the board variables
     */
    private void setup() {
        mRow = 0;
        mColumn = 0;
        mDirection = Direction.RIGHT;
        mBoard = new int[mColumns][mRows];
        mBoard[mColumn][mRow] = 1;
    }

    /**
     * Check if you have gone through all the points on the board
     *
     * @return boolean
     */
    private boolean isNotFinished() {
        for (int[] rows : mBoard) {
            for (int row : rows) {
                if (row == 0) return true;
            }
        }
        return false;
    }

    /**
     * Advance to the next valid position
     */
    private void moveToNextPoint() {
        int nextRow;
        int nextColumn;

        switch (mDirection) {
            case RIGHT:
                nextRow = mRow + 1;

                if (nextRow == mRows || mBoard[mColumn][nextRow] == 1) {
                    mDirection = Direction.DOWN;
                    moveToNextPoint();
                } else if (mBoard[mColumn][nextRow] == 0) {
                    mRow = nextRow;
                }
                break;
            case LEFT:
                nextRow = mRow - 1;

                if (nextRow < 0 || mBoard[mColumn][nextRow] == 1) {
                    mDirection = Direction.UP;
                    moveToNextPoint();
                } else if (mBoard[mColumn][nextRow] == 0) {
                    mRow = nextRow;
                }
                break;
            case DOWN:
                nextColumn = mColumn + 1;

                if (nextColumn == mColumns || mBoard[nextColumn][mRow] == 1) {
                    mDirection = Direction.LEFT;
                    moveToNextPoint();
                } else if (mBoard[nextColumn][mRow] == 0) {
                    mColumn = nextColumn;
                }
                break;
            case UP:
                nextColumn = mColumn - 1;

                if (nextColumn == 0 || mBoard[nextColumn][mRow] == 1) {
                    mDirection = Direction.RIGHT;
                    moveToNextPoint();
                } else if (mBoard[nextColumn][mRow] == 0) {
                    mColumn = nextColumn;
                }
                break;
        }

        mBoard[mColumn][mRow] = 1;
        // printBoard();
    }

    /**
     * Print the board
     */
    private void printBoard() {
        System.out.println("===========");
        for (int[] columns : mBoard) {
            for (int row : columns) {
                System.out.print(row + "\t");
            }
            System.out.println("\n");
        }
    }

    /**
     * Print the current direction
     */
    private void printDirection() {
        System.out.println(mDirection.description);
    }
}