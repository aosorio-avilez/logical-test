package com.dacodes;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Console {

    private final BufferedReader mReader;

    /**
     * Constructor
     */
    public Console() {
        mReader = new BufferedReader(new InputStreamReader(System.in));
    }

    /**
     * Get the number of test cases
     *
     * @return int
     */
    public int getNumberOfTestCases() {
        System.out.print("Enter the number of cases: ");
        try {
            return Integer.parseInt(mReader.readLine());
        } catch (Exception e) {
            throw new RuntimeException("Incorrect input format for text cases");
        }
    }

    /**
     * Get the dimensions of the array
     *
     * @return int[]
     */
    public int[] getDimensions() {
        System.out.print("Enter number of rows and columns separate for space: ");
        try {
            String line = mReader.readLine();
            String[] values = line.split(" ");
            return new int[]{Integer.parseInt(values[0]), Integer.parseInt(values[1])};
        } catch (Exception e) {
            throw new RuntimeException("Incorrect input format for dimension");
        }
    }
}
